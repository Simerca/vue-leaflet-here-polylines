# vue-leaflet-here
Ce projet vient de mon article sur dev.to
https://dev.to/simerca

Hi 👋 !
**Thank you** for your feedback from my last post !
Take 1 minutes to like this too, and share me your feedback in the comments section !

**So !**

Do you want to display **14000+ Polylines** inside your browser without make your CPU **launch on the moon** ?

**Liftoff !**

Start a blank VueJs project where you want
```
vue create app
```

next, install **Leaflet** packages and **Vue2-Leaflet**
```
npm install leaflet
npm install vue2-leaflet
```

### Ok we can start

**the first step is to initialise Map inside a component**

```
<template>
  <div>
    <l-map
      :zoom="zoom"
      :center="center"
      :options="mapOptions"
      style="height: 100vh"
    >
        <l-tile-layer 
          :url="url"
          :attribution="attribution"
        />
    </l-map>
  </div>
</template>

import { LMap, LTileLayer } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css'

export default {
  components:{
    LMap,
    LTileLayer,
  },
  data(){
    return {
      zoom: 8,
      center: [44.8, -0.6],
      url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution:
        '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
      mapOptions: {
        zoomSnap: 0.5
      }
    }
  }
}
```

*This is the simple thing !*

Now...

### Did you know HERE ?
Here is for me, the best provider of map's datas, I invite you to view they documentation here:
https://developper.here.com

And they have create a little algorithm to encode/decode Polyline, the benefit of this thing it's how fast it will be decoded.

for exemples I have create a multiple polylines with lots of bounds, **about 14000+**.

HERE encode this on a simple string like this:
```
https://gitlab.com/-/snippets/2020388
```
**this file is very light, 58 ko !!!**

so import this script inside a folder like
src/assets/flexiblepolyline.js
```
https://gitlab.com/-/snippets/2020385
```

and import it inside your components
```
import H from '../assets/js/flexiblepolyline.js'
```

get the exemple file of datas.json and import it too

```
import datas from '../assets/datas.json';
```

**Now lets started to create the Polyline on your map.**

add **LGeoJson** inside your **vue2-leaflet** import.
```
import { LMap, LTileLayer, LGeoJson } from 'vue2-leaflet';
```

and add components inside the **<l-map> components** on your template like this:
```
<l-geo-json
        v-for="(data,i ) in datas" :key="i"
        :geojson="decode(data)"
        />
```
*v-for datas is to loop inside the datas.json file.*

set your components params like this
```
components:{
    LMap,
    LTileLayer,
    LGeoJson
  }
```
add this methods:
```
methods:{
    decode(str){
      let lines = H.decode(str);
      return {
          "type": "Feature",
          "geometry": {
              "type": "LineString",
              "coordinates": lines.polyline
          }
      };
    }
  },
```
*it will be decode the encoded string and return the polyline* 

**Enjoy the result 🍾 !**
![Alt Text](https://dev-to-uploads.s3.amazonaws.com/i/l404zl8jsdi64aijv8o8.gif)

And for the lazy people:

# vue-leaflet-here

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
